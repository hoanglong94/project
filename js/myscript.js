$(function() {
    $(".khungAnhCrop img")
        .each(function() {
            $(this)
                .removeClass("wide tall")
                .addClass((this.width / this.height > $(this).parent().width() / $(this).parent().height()) ?
                    "wide" :
                    "tall");
        });


        $('#openMenu').click(function(){
        	$('#nav-top,#header').toggleClass('open');

        })
        $('li.collapsed').click(function() {
        	$('li.collapsed').addClass('active');
        	
        });
         $('li.collapsed.active').click(function() {
        	$('li.collapsed.active').removeClass('active');
        });
    $('.slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider-nav'
	});
	$('.slider-nav').slick({
	  	slidesToShow: 3,
	  	slidesToScroll: 1,
	  	asNavFor: '.slider-for',
	 	dots: false,
	  	centerMode: true,
	  	focusOnSelect: true,
	  	autoplay: true,
  		autoplaySpeed: 2000
	});
	var wOW = $(window).outerWidth();
	if(wOW<768){
		$('.group').addClass('mobile-slider');
		$('.mobile-slider .slick-track').css('width', '100%');
		$('.mobile-slider').slick();
		//
		$('.why-body').addClass('whySlider');
		$('.whySlider .slick-track').css('width', '100%');
		$('.whySlider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			dots:true
		});
	}
	else{
		$('.mobile-slider').slick('unslick');
		$('.group').removeClass('mobile-slider');
		$('.mobile-slider .slick-track').css('width', 'auto');
		//
		$('.whySlider').slick('unslick');
		$('.why-body').removeClass('whySlider');
		$('.whySlider .slick-track').css('width', 'auto');
		
	}
	window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        var height = (window.innerHeight - 10);
        if (document.body.scrollTop > height || document.documentElement.scrollTop > height) {
            document.getElementById("btn-goto-top").style.display = "block";
        } else {
            document.getElementById("btn-goto-top").style.display = "none";
        }
    }
    function topFunction() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
});
$(window).resize(function(){
	var wOW = $(window).outerWidth();
	if(wOW<768){
		$('.group').addClass('mobile-slider');
		$('.mobile-slider .slick-track').css('width', '100%');
		$('.mobile-slider').slick();
		//
		$('.why-body').addClass('whySlider');
		$('.whySlider .slick-track').css('width', '100%');
		$('.whySlider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			dots:false
		});
	}
	else{
		$('.mobile-slider').slick('unslick');
		$('.group').removeClass('mobile-slider');
		$('.mobile-slider .slick-track').css('width', 'auto');
		//
		$('.whySlider').slick('unslick');
		$('.why-body').removeClass('whySlider');
		$('.whySlider .slick-track').css('width', 'auto');
		
	}
})